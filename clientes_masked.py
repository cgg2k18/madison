print("ID,Nombre,Email, Facturado, Localidad")

# IDs

id1 = 1
id2 = 2
id3 = 3
id4 = 4
id5 = 5

# Nombres

n1 = "Juan_Martinez"
n1 = "XXXX_XXXXXXXX,"

n2 = "Francisco_Gomez"
n2 = "XXXXXXXXX_XXXXX,"

n3 = "Gerardo_Puente"
n3 = "XXXXXXX_XXXXXX,"

n4 = "Javier_Cruz"
n4 = "XXXXXX_XXXX,"

n5 = "Fernando_Martin"
n5 = "XXXXXXXX_XXXXXX,"

# Email

e1 = "juan@mail.com"
e1 = "XXXX@XXXX.XXX,"

e2 = "paco@my-mail.com"
e2 = "XXXX@XX-XXXX.XXX,"

e3 = "gerardo-puente@mail.com"
e3 = "XXXXXXX-XXXXXX@XXXX.XXX,"

e4 = "jcruz@mail.com"
e4 = "XXXXX@XXXX.XXX,"

e5 = "fm@mail.com"
e5 = "XX@XXXX.XXX,"

# Facturado

f1 = 15000
f2 = 20000
f3 = 0
f4 = 12400
f5 = 53000
f6 = 5

ft = int(f1) + int(f2) + int(f3) + int(f4) + int(f5)
fm = ft / f6

f1 = fm
f2 = fm
f3 = fm
f4 = fm
f5 = fm

# Localidad

l1 = "Valladolid"
l2 = "Salamanca"
l3 = "Barcelona"
l4 = "Madrid"
l5 = "Soria"

# Resultado

print(id1, n1, e1, f1, l1 )
print(id2, n2, e2, f2, l2 )
print(id3, n3, e3, f3, l3 )
print(id4, n4, e4, f4, l4 )
print(id5, n5, e5, f5, l5 )

git clone https://cgg2k18@bitbucket.org/cgg2k18/madison.git